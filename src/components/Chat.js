import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  addMessage,
  getMessages,
  deleteMessage,
  hideLoader,
} from '../redux/actions';
import { Header } from './Header';
import MessageList from './MessageList';
import { Form } from './Form';
import { Preloader } from './Preloader';
import { EditWindow } from './EditWindow';

const Chat = ({ url }) => {
  const dispatch = useDispatch();
  const fetchedMessages = useSelector((state) => state.chat.messages);
  const show = useSelector((state) => state.chat.preloader);

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        dispatch(getMessages(data));
        dispatch(hideLoader());
      });
  }, [dispatch, url]);

  const handleSubmitMessage = (message) => dispatch(addMessage(message));
  const handleDeleteMessage = (id) => dispatch(deleteMessage(id));

  const content = (
    <div>
      <Preloader />
      <Header info={fetchedMessages} />
      <MessageList messages={fetchedMessages} onDelete={handleDeleteMessage} />
      <EditWindow />
      <Form handleSubmit={handleSubmitMessage} />
    </div>
  );

  return (
    <div className="chat" style={style}>
      {show ? <Preloader /> : content}
    </div>
  );
};

const style = {
  backgroundColor: '#F4F1DE',
  padding: '50px 20vw',
  fontFamily: '"Quicksand", sans-serif',
};

export default Chat;
