import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IconContext } from 'react-icons';
import {
  AiFillHeart,
  AiOutlineHeart,
  AiOutlineDelete,
  AiFillSetting,
  AiOutlineSetting,
} from 'react-icons/ai';
import { Avatar } from './Avatar';
import { showEditWindow } from '../redux/actions';

export const Message = ({ message, onDelete }) => {
  const [liked, setLiked] = useState(false);
  const dispatch = useDispatch();

  const { show } = useSelector((state) => ({
    show: state.chat.editModal.show,
  }));

  const time = new Date(message.createdAt).toLocaleTimeString('ru-Ru', {
    timeStyle: 'short',
  });

  const onLike = () => {
    liked ? setLiked(false) : setLiked(true);
  };

  const onEdit = () => {
    dispatch(showEditWindow(message));
  };

  const ownMsg = (
    <div className="own-message" style={style.ownMessage.container}>
      <div
        className="message-delete"
        style={style.ownMessage.icon}
        onClick={() => onDelete(message.id)}
      >
        <IconContext.Provider value={{ color: '#fff' }}>
          <AiOutlineDelete />
        </IconContext.Provider>
      </div>
      <div
        className="message-edit"
        style={style.ownMessage.icon}
        onClick={onEdit}
      >
        <IconContext.Provider value={{ color: '#fff' }}>
          {show ? <AiFillSetting /> : <AiOutlineSetting />}
        </IconContext.Provider>
      </div>
      <div style={style.ownMessage.textContainer}>
        <div className="message-text">{message.text}</div>
        <div className="message-time">{time}</div>
      </div>
    </div>
  );

  const msg = (
    <div className="message" style={style.message.container}>
      <Avatar className="message-user-avatar" src={message.avatar} />
      <div style={style.message.textContainer}>
        <div className="message-user-name" style={style.message.name}>
          {message.user}
        </div>
        <div className="message-text">{message.text}</div>
        <div className="message-time">{time}</div>
      </div>
      <IconContext.Provider value={{ color: '#fff' }}>
        <div
          className={liked ? 'message-liked' : 'message-like'}
          style={{ ...style.message.like }}
          onClick={() => onLike()}
        >
          {liked ? <AiFillHeart /> : <AiOutlineHeart />}
        </div>
      </IconContext.Provider>
    </div>
  );

  return message.user === 'Nik' ? ownMsg : msg;
};

const style = {
  ownMessage: {
    container: {
      display: 'flex',
      color: '#fff',
      background: '#B9396D',
      border: 'black 1px solid',
      borderRight: 'none',
      maxWidth: '35vw',
      minHeight: '100px',
      margin: '10px 0 10px auto',
    },
    textContainer: { margin: '15px 10px 0 auto' },
    name: {
      fontSize: '14px',
      fontWeight: 'bold',
      float: 'right',
      margin: '10px 0 5px',
    },
    icon: { fontSize: '25px', margin: 'auto 0 0 4px' },
  },
  message: {
    container: {
      display: 'flex',
      color: '#fff',
      backgroundColor: '#D13242',
      border: 'black 1px solid',
      borderLeft: 'none',
      maxWidth: '35vw',
      minHeight: '20px',
      margin: '10px 0',
    },
    textContainer: { marginLeft: '10px' },
    name: {
      fontSize: '14px',
      fontWeight: 'bold',
      margin: '10px 0 5px',
    },
    like: {
      fontSize: '25px',
      margin: 'auto 7px 0 auto',
      float: 'right',
    },
  },
};
