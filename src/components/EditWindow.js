import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { hideEditWindow, editMessage } from '../redux/actions';

export const EditWindow = () => {
  const { show, message } = useSelector((state) => ({
    show: state.chat.editModal.show,
    message: state.chat.editModal.message,
  }));

  const [input, setInput] = useState('');
  const dispatch = useDispatch();

  const onEditMessage = () => {
    const newMessage = {
      ...message,
      text: input,
      editedAt: Date.now(),
    };
    dispatch(editMessage(newMessage));
    dispatch(hideEditWindow());
  };

  useEffect(() => {
    if (message) setInput(message.text);
  }, [message]);

  const content = (
    <form
      className={`edit-message-modal ${show ? 'modal-shown' : ''}`}
      style={style.container}
      action="submit"
      onSubmit={() => onEditMessage()}
    >
      <div style={style.content.container}>
        <div style={style.content.title}>Edit Your Message</div>
        <textarea
          className="edit-message-input"
          style={style.content.input}
          value={input}
          onChange={(e) => setInput(e.target.value)}
          type="text"
        />
        <div>
          <button
            className="edit-message-button"
            style={style.content.button}
            type="submit"
          >
            Send
          </button>
          <button
            className="edit-message-close"
            style={style.content.button}
            type="button"
            onClick={() => dispatch(hideEditWindow())}
          >
            Close
          </button>
        </div>
      </div>
    </form>
  );

  return show ? content : null;
};

const style = {
  container: {
    display: 'flex',
    position: 'fixed',
    top: '0',
    bottom: '0',
    left: '0',
    right: '0',
    justifyContent: 'center',
    background: 'rgba(0, 0, 0, 0.7)',
  },
  content: {
    container: {
      border: '#fff 2px solid',
      borderRadius: '25px',
      padding: '30px',
      marginTop: '200px',
      background: '#D13242',
      color: '#fff',
      maxHeight: '190px',
    },
    title: {
      marginBottom: '20px',
    },
    input: {
      width: '200px',
      height: '100px',
      padding: '5px',
      resize: 'none',
      border: 'none',
      borderRadius: '5px',
    },
    button: {
      margin: '5px 10px 0 0',
      padding: '10px',
      border: 'none',
      borderRadius: '5px',
      background: '#fff',
    },
  },
};
