import {
  GET_MESSAGES,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  SHOW_EDIT_WINDOW,
  HIDE_EDIT_WINDOW,
  SHOW_LOADER,
  HIDE_LOADER,
} from './types';

export const chatReducer = (state = [], { type, payload }) => {
  switch (type) {
    case GET_MESSAGES:
      return (state = payload);
    case ADD_MESSAGE:
      return (state = [...state, payload]);
    case DELETE_MESSAGE: {
      const newMessages = state.filter((message) => message.id !== payload);
      return (state = newMessages);
    }
    case EDIT_MESSAGE: {
      const message = payload;

      const newMessages = state.map((oldMessage) => {
        if (oldMessage.id === message.id) {
          return message;
        }
        return oldMessage;
      });
      return (state = newMessages);
    }
    default:
      return state;
  }
};

export const editWindowReducer = (
  state = { show: false, message: null },
  { type, payload }
) => {
  switch (type) {
    case SHOW_EDIT_WINDOW: {
      state = { show: true, message: payload };
      return state;
    }
    case HIDE_EDIT_WINDOW: {
      state = { show: false, message: null };
      return state;
    }
    default:
      return state;
  }
};

export const preloaderReducer = (state = true, { type }) => {
  switch (type) {
    case SHOW_LOADER:
      state = true;
      return state;
    case HIDE_LOADER:
      state = false;
      return state;
    default:
      return state;
  }
};
