import {
  GET_MESSAGES,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  SHOW_EDIT_WINDOW,
  HIDE_EDIT_WINDOW,
  SHOW_LOADER,
  HIDE_LOADER,
} from './types';

export const getMessages = (messages) => {
  return {
    type: GET_MESSAGES,
    payload: messages,
  };
};

export const addMessage = (message) => {
  return {
    type: ADD_MESSAGE,
    payload: message,
  };
};

export const deleteMessage = (id) => {
  return {
    type: DELETE_MESSAGE,
    payload: id,
  };
};

export const editMessage = (message) => {
  return {
    type: EDIT_MESSAGE,
    payload: message,
  };
};

export function showEditWindow(message) {
  return {
    type: SHOW_EDIT_WINDOW,
    payload: message,
  };
}

export function hideEditWindow() {
  return {
    type: HIDE_EDIT_WINDOW,
  };
}

export function showLoader() {
  return {
    type: SHOW_LOADER,
  };
}

export function hideLoader() {
  return {
    type: HIDE_LOADER,
  };
}
