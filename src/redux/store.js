import { createStore, combineReducers } from 'redux';
import reducers from './reducers';

export const rootReducer = combineReducers({ chat: reducers });

const store = createStore(rootReducer);

export default store;
