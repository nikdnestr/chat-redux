import { combineReducers } from 'redux';
import {
  chatReducer,
  editWindowReducer,
  preloaderReducer,
} from './rootReducer';

const reducers = combineReducers({
  messages: chatReducer,
  editModal: editWindowReducer,
  preloader: preloaderReducer,
});

export default reducers;
